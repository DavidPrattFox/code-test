#!/bin/bash

#### Install Node.js and NPM ####

sudo yum install -y ruby

sudo gem install sass

sudo gem install compass

su - vagrant -c "curl https://raw.githubusercontent.com/creationix/nvm/v0.13.1/install.sh | bash"

su - vagrant -c "source ~/.bash_profile"

su - vagrant -c "nvm install v6.3.1"

su - vagrant -c "nvm alias default 6.3.1"

su - vagrant -c "npm install -g grunt-cli bower yo generator-karma generator-angular"